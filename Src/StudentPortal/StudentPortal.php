<?php
/**
 * Created by PhpStorm.
 * User: saman
 * Date: 11/1/2017
 * Time: 3:49 PM
 */

namespace App\StudentPortal;


use App\Model\Database;
use PDO;


class StudentPortal extends Database
{
    public $Name,$Student_id,$Current_Semester;
    public $Result_1_1,$Result_1_2,$Result_2_1,$Result_2_2,$Result_3_1,$Result_3_2,$Result_4_1,$Result_4_2;
    public $Advisor,$Any_backlogs;


    public function setData($postArray)
    {
        if (array_key_exists("Name", $postArray))
            $this->Name = $postArray['Name'];

        if (array_key_exists("StudentID", $postArray))
            $this->Student_id = $postArray['StudentID'];

       /* if (array_key_exists("Password", $postArray))
            $this->password = $postArray['Password']; */

        if (array_key_exists("Level-Term", $postArray))
            $this->Current_Semester = $postArray['Level-Term'];

        if (array_key_exists("Result 1-1", $postArray))
            $this->Result_1_1 = $postArray['Result 1-1'];
        if (array_key_exists("Result 1-2", $postArray))
            $this->Result_1_2 = $postArray['Result 1-2'];
        if (array_key_exists("Result 2-1", $postArray))
            $this->Result_2_1 = $postArray['Result 2-1'];
        if (array_key_exists("Result 2-2", $postArray))
            $this->Result_2_2 = $postArray['Result 2-2'];
        if (array_key_exists("Result 3-1", $postArray))
            $this->Result_3_1 = $postArray['Result 3-1'];
        if (array_key_exists("Result 3-2", $postArray))
            $this->Result_3_2 = $postArray['Result 3-2'];
        if (array_key_exists("Result 4-1", $postArray))
            $this->Result_4_1 = $postArray['Result 4-1'];
        if (array_key_exists("Result 4-2", $postArray))
            $this->Result_4_2 = $postArray['Result 4-2'];

        if (array_key_exists("Advisor", $postArray))
            $this->Advisor = $postArray['Advisor'];

        if (array_key_exists("Backlogs", $postArray))
            $this->Any_backlogs = $postArray['Backlogs'];



        return $this;
    }

  /*  public function store()
    {$sqlQuery = "INSERT INTO users(name,password,email) VALUES (?,?,?)";
        $dataArray = [$this->name, $this->password, $this->email];

        $sth = $this->dbh->prepare($sqlQuery);

        $sth->execute($dataArray);
    }

    public function signup()
    {
        $sqlQuery = "INSERT INTO student_account(name,student_id,email,password,level,term,dob,phone_number,present_address,permanent_address) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $dataArray = [$this->name, $this->student_id, $this->email, $this->password, $this->level, $this->term, $this->dob, $this->phone_number, $this->present_address, $this->permanent_address];

        $sth = $this->dbh->prepare($sqlQuery);

        $sth->execute($dataArray);
    }


    public function login()
    {

        $sqlQuery = "SELECT * FROM student_account WHERE name='$this->name' AND password='$this->password'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    } */

    public function view(){

        $sqlQuery = "Select * from student where name='$this->Name'";

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData =  $sth->fetch();

        return $oneData;
}

}