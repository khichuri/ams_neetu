<?php
/**
 * Created by PhpStorm.
 * User: saman
 * Date: 11/1/2017
 * Time: 3:38 PM
 */

namespace App\Model;
use PDO,PDOException;


class Database
{

    public $dbh;

    public function __construct()
    {
        try{

            $this->dbh = new PDO("mysql:host=localhost;dbname=advisory_system","root","");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

            echo "db successful";
        }

        catch (PDOException $error){
            echo $error->getMessage();
        }
    }

}