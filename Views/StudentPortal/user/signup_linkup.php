<?php

require_once("../../../vendor/autoload.php");

$obj = new \App\StudentPortal\StudentPortal();
$obj->setData($_POST)->signup();

header("Location:login.php");