<?php

include_once('../../../vendor/autoload.php');

if(!isset($_SESSION)) session_start();

use App\Message\Message;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Account</title>


    <link rel="stylesheet" href="../../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/styling/style.css">

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>

<body>

<div class="col-md-2" ></div>

<div class="col-md-8" id="base" style="margin-top: 50px; margin-bottom: 50px;height: 900px">
    <form action="signup_linkup.php" method="post">

        <fieldset>
            <h2 style="text-align: center"><b> Student Account Form </b></h2>

            <div class="form-group">
                <label for="Name"> Name </label>
                <input type="text" class="form-control" name="Name" placeholder="Enter the user name here....">
            </div>

            <div class="form-group">
                <label for="StudentID">Student ID </label>
                <input type="text" class="form-control" name="StudentID" placeholder="Enter the user id here....">
            </div>

            <div class="form-group">
                <label for="Email">Email </label>
                <input type="email" class="form-control" name="Email" placeholder="Enter the email id here....">
            </div>

            <div class="form-group">
                <label for="Password">Password </label>
                <input type="text" class="form-control" name="Password" placeholder="Enter the user id here....">
            </div>

            <div class="form-group">
                <label for="Level"> Level </label>
                <input type="text"  class="form-control" name="Level" placeholder="Enter the level here....">
            </div>

            <div class="form-group">
                <label for="Term"> Term </label>
                <input type="text"  class="form-control" name="Term" placeholder="Enter the term here....">
            </div>

            <div class="form-group">
                <label for="Date_of_birth">Date Of Birth: </label>
                <input type="date"  class="form-control" name="Date_of_birth" placeholder="DD/MM/YY">
            </div>

            <div class="form-group">
                <label for="Phone_Number"> Phone Number </label>
                <input type="text"  class="form-control" name="Phone_Number" placeholder="Enter the phone number here....">
            </div>

            <div class="form-group">
                <label for="Present_Address"> Present Address </label>
                <input type="text"  class="form-control" name="Present_Address" placeholder="Enter the present address here....">
            </div>

            <div class="form-group">
                <label for="Permanent_Address"> Permanent Address </label>
                <input type="text"  class="form-control" name="Permanent_Address" placeholder="Enter the permanent address here....">
            </div>

            <div class="form-group">
                <a href="#result"></a>
            </div>


            <button type="submit" class="btn btn-success">Submit</button>

        </fieldset>
    </form>

</div>

<div class="col-md-2"></div>

</body>
</html>