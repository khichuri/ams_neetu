<?php
include_once('../../../vendor/autoload.php');

if(!isset($_SESSION)) session_start();

use App\Message\Message;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/styling/style.css">


</head>
<body id="background_image">

        <div class="col-lg-6"  id='base'>

    <h1 style="text-align: center"> Login To your account </h1>

    <form action="login_linkup.php" method="post">


        <br> <br>


        <div class="form-group">
            <label for="Name">Username</label>
            <input type="text" class="form-control" name="Name">
        </div>

        <br>



        <div class="form-group">
            <label for="Password">Password</label>
            <input type="password" class="form-control" name="Password" >
        </div>


        <br>


        <button type="submit" class="btn btn-primary">Login</button>




    </form>


</div>

</body>
</html>