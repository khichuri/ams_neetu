<?php

require_once ("../../vendor/autoload.php");

if(!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage</title>

    <link rel="stylesheet" href="../../Resources/styling/style.css">
    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">


</head>

<!---->
<body>
<div class="container col-lg-12" id="background">


    <div class="container" id="homepage_title">
        <img src="../../Resources/Images/homepage/cuetlogo.png" style="margin-top: 15px">
        <h1> Advisory Management System </h1>
    </div>

    <div class="row" style="height: 50px;"></div>

    <div class = "container">
    <div class="row">
        <div class="col-lg-5"></div>
        <div class = "col-lg-4">
            <input type="button" class="btn btn-success" style="padding: 10px 16px 10px 16px; align-self: center;" value="Login as Advisor">
        </div>
        <div class="col-lg-4"></div>
    </div>
    </div>

    <div class="row" style="height: 20px;"></div>

    <div class = "container">
        <div class="row">
            <div class="col-lg-5"></div>
            <div class = "col-lg-4">
                <input type="button" class="btn btn-success" style="padding: 10px 15px 10px 15px; position: center;" value="Login as Student">
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>

    <div class="row" style="height: 20px;"></div>

    <div class = "container">
        <div class="row">
            <div class="col-lg-5"></div>
            <div class = "col-lg-4">
                <input type="button" class="btn btn-success" style="padding: 10px 25px 10px 25px; position: center;" value="Login as Staff">
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>

    <!---<div class="navbar">

        <ul>
            <li> <a class="active" href="user/login.php" class="w3-bar-item w3-button"> Login </a></li>
            <li> <a href="user/signup.php" class="w3-bar-item w3-button"> Sign Up </a></li>

        </ul>
    </div>
    -->




</div>

</body>
</html>
