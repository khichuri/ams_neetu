<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Account</title>


    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Resources/styling/style.css">

    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>


<div class="container" id="homepage_title">
    <h1> Student Account </h1>
</div>

<div class="navbar">

    <ul>
        <li> <a class="active" href="#login"> Login </a></li>
        <li> <a href="#signup"> Sign Up </a></li>

    </ul>
</div>

</body>
</html>